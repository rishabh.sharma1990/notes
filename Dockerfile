FROM node:latest

RUN mkdir -p /app/src

WORKDIR /app/src

COPY package.json .

RUN npm install

COPY . .

RUN yarn test

EXPOSE 3000

RUN npm start