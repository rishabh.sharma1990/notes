import styled from "styled-components/macro";

const Button = styled.button`
  border: none;
  background: none;
  cursor: pointer;
`;

export default Button;
